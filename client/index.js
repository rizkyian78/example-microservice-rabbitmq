const express = require('express')
const AMQP = require("./amqp-service")

const app = express()
const amqpService = new AMQP()

app.use(express.json())

app.post('/', function (req, res) {
    console.log(amqpService)
    amqpService.publishMessage(req.body, "<<<")
  res.send('Hello World')
})

amqpService.startConsume()

app.listen(3000, () => console.log("Server running at port 3000"))