const amqplib = require('amqplib');
const queue = 'queue';


class AmqpService {
    async connect() {
        const conn = await amqplib.connect('amqp://localhost')
        conn.on('error', (err) => {
         console.error('Connection error', err)
         throw new Error("Cannot Connect Rabbitmq")
        })
        return conn
    }
    publishMessage(payload) {
       this.connect().then(async channelWrapper => {
        const channel = await channelWrapper.createChannel();
        const jsonBody = JSON.stringify(payload)
        await channel.assertQueue(queue, { durable: false });
        channel.sendToQueue(queue, Buffer.from(jsonBody))
        console.log(`publish message: ${jsonBody} `)
       })
    }

    startConsume() {
        this.consumeMessage("server")
    }
    consumeMessage(queue) {
        this.connect().then(async channelWrapper => {
            const channel = await channelWrapper.createChannel();
            await channel.assertQueue(queue, { durable: true });
            channel.consume(queue, msg => {
                console.log(`receive message: ${msg.content.toString()} `)
                channel.ack(msg)
            })
        })

    }
}


module.exports = AmqpService