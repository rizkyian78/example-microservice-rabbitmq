module rizkyian78/server

go 1.20

require github.com/streadway/amqp v1.1.0

require (
	github.com/rabbitmq/amqp091-go v1.7.0 // indirect
	github.com/wagslane/go-rabbitmq v0.12.4 // indirect
)
