package utils

import (
	"fmt"
	"log"

	"github.com/streadway/amqp"
)

type Producer struct {
	ch    *amqp.Channel
	conn  *amqp.Connection
	queue string
	done  chan error
}

func NewHandlerProducer(host string, queue string) (*Producer, error) {
	p := &Producer{
		conn: nil,
		ch:   nil,
		done: make(chan error),

		queue: queue,
	}

	var err error
	p.conn, err = amqp.Dial(host)
	if err != nil {
		return nil, fmt.Errorf("unable to initialize amqp producer: %w", err)
	}

	go func() {
		fmt.Printf("closing: %s", <-p.conn.NotifyClose(make(chan *amqp.Error)))
	}()

	p.ch, err = p.conn.Channel()
	if err != nil {
		return nil, fmt.Errorf("unable to get amqp channel: %w", err)
	}

	if _, err := p.ch.QueueDeclare(queue, true, false, false, false, nil); err != nil {
		return nil, err
	}

	if err := p.ch.Confirm(false); err != nil {
		return nil, fmt.Errorf("Channel could not be put into confirm mode: %s", err)
	}

	return p, nil
}
func (p *Producer) PublishMessage(body string) error {
	err := p.ch.Publish(
		"",      // exchange
		p.queue, // routing key
		false,   // mandatory
		false,   // immediate
		amqp.Publishing{
			ContentType:  "text/plain",
			Body:         []byte(body),
			DeliveryMode: amqp.Persistent,
			Priority:     0,
		})

	if err != nil {
		return err
	}
	log.Printf("send back to queue: %s \n", p.queue)
	log.Printf("with payload: %s \n", body)

	return nil
}
