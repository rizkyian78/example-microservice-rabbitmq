package utils

import (
	"fmt"
	"os"
	"time"

	"github.com/streadway/amqp"
)

type Handler struct {
	ch    *amqp.Channel
	conn  *amqp.Connection
	queue string
}

func NewHandler(host string, queue string) (*Handler, error) {
	conn, err := amqp.Dial(host)
	if err != nil {
		return &Handler{}, err
	}
	ch, err := conn.Channel()
	if err != nil {
		return &Handler{}, err
	}
	go func() {
		if err := <-ch.NotifyClose(make(chan *amqp.Error)); err != nil {
			fmt.Printf("AMQP channel closed: %s", err)
			time.Sleep(5 * time.Second)
			os.Exit(9)
		}
	}()

	go func() {
		if err := <-conn.NotifyClose(make(chan *amqp.Error)); err != nil {
			fmt.Printf("AMQP channel closed: %s", err)
			time.Sleep(5 * time.Second)
			os.Exit(9)
		}
	}()

	fmt.Printf("Ready to consume: %s \n", queue)

	return &Handler{
		ch:    ch,
		conn:  conn,
		queue: queue,
	}, nil
}

func (h *Handler) ConsumeQueue(deliveries chan<- string, errChan chan<- error) error {

	q, err := h.ch.QueueDeclare(
		h.queue, // name
		false,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	if err != nil {
		return err
	}

	messageChannel, err := h.ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	if err != nil {
		return err
	}
	for d := range messageChannel {
		//fmt.Printf(
		//	"got %dB delivery: [%v] %q",
		//	len(d.Body),
		//	d.DeliveryTag,
		//	d.Body,
		//)
		// d.Ack(false)
		deliveries <- string(d.Body)
	}
	return nil
}
