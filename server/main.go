package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"rizkyian78/server/utils"
	"time"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main() {
	handler, err := utils.NewHandler("amqp://guest:guest@localhost:5672/", "queue")
	failOnError(err, "error")
	cronTicker, err := utils.NewCronTimer("5s", time.Now().Location())
	failOnError(err, "error")
	queueProducer, err := utils.NewHandlerProducer("amqp://guest:guest@localhost:5672/", "server")
	failOnError(err, "error")

	// payload := make(chan amqp.Delivery)
	closeChan := make(chan os.Signal)
	go signal.Notify(closeChan, os.Interrupt)

	queueConsumer := make(chan string)
	errChan := make(chan error)

	go handler.ConsumeQueue(queueConsumer, errChan)

loopMessage:
	for {
		select {
		case payload := <-queueConsumer:
			fmt.Println("payload", payload)
		case <-closeChan:
			fmt.Println("catch interuption signal")
			time.Sleep(time.Second)
			break loopMessage
		case <-cronTicker.TimeTicker.C:
			fmt.Println("Send queue from server")
			go queueProducer.PublishMessage("{message:'Halo dari server'}")
			cronTicker.UpdateCronTimer()
		case <-errChan:
			fmt.Println("get an error")
			time.Sleep(time.Second)
			break loopMessage
		}
	}
}
